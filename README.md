# README #

Le but de ce test est de mettre en place une classe utilitaire qui parse les fichiers XML.

## Etape 1

```
java -jar technical-test-1.0.0-SNAPSHOT.jar --file template.xml --path block.link 
```

Cela doit retourner dans la console : 
```
Valeur du lien dans le block
```

## Etape 2

```
java -jar technical-test-1.0.0-SNAPSHOT.jar --file template.xml --path block.link --value newValue --output generated.xml
```

Cela doit retourner un fichier generated.xml qui contient la m�me diff�rence qu'entre template.xml et template-step-2.xml


## Etape 3

```
java -jar technical-test-1.0.0-SNAPSHOT.jar --file template.xml --path data.itemData[0].description --value newDescription --output generated3.xml
```

Cela doit retourner un fichier generated.xml qui contient la m�me diff�rence qu'entre template.xml et template-step-3.xml


