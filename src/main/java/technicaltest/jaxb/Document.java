package technicaltest.jaxb;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Document")
public class Document {
	private List<Zone> mainZones;

	public List<Zone> getMainZones() {
		return mainZones;
	}

	@XmlElement(name = "mainZone")
	public void setMainZones(List<Zone> mainZones) {
		this.mainZones = mainZones;
	}

}
