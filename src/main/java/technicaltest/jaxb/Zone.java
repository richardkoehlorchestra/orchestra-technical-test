package technicaltest.jaxb;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = { "code", "type", "label", "zones", "value" })
@XmlRootElement(name = "zone")
public class Zone {

	private String code;

	private String type;

	private String label;

	private String value;

	private List<Zone> zones;

	public String getCode() {
		return code;
	}

	@XmlAttribute
	public void setCode(String code) {
		this.code = code;
	}

	public String getType() {
		return type;
	}

	@XmlAttribute
	public void setType(String type) {
		this.type = type;
	}

	public String getLabel() {
		return label;
	}

	@XmlAttribute
	public void setLabel(String label) {
		this.label = label;
	}

	public String getValue() {
		return value;
	}

	@XmlElement(name = "value")
	public void setValue(String value) {
		this.value = value;
	}

	public List<Zone> getZones() {
		return zones;
	}

	@XmlElementWrapper(name = "zones")
	@XmlElement(name = "zone")
	public void setZones(List<Zone> zones) {
		this.zones = zones;
	}

}
