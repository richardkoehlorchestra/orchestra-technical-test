package technicaltest.util;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 * Utility class for XML
 * 
 * @author Aubin
 *
 * @param <T>
 */
public class XmlUtil<T> {

	private Class<T> typeParameterClass;

	public XmlUtil(Class<T> typeParameterClass) {
		this.typeParameterClass = typeParameterClass;
	}

	/**
	 * Parse an XML file to JAXB
	 * @param file
	 * @return
	 * @throws JAXBException
	 */
	public T parse(File file) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(typeParameterClass);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		return (T) jaxbUnmarshaller.unmarshal(file);
	}

	public void generateXml(T jaxbObject, File file) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(typeParameterClass);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		jaxbMarshaller.marshal(jaxbObject, file);
	}
	public Marshaller getMarshaller() throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(typeParameterClass);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		return jaxbMarshaller;
	}
}
