package technicaltest.util;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import technicaltest.jaxb.Document;
import technicaltest.jaxb.Zone;

public class DocumentUtil {

	private static final String ARRAY_ELEMENT_REGEX = "[0-9]+";

	/**
	 * Returns the node value found by path
	 * 
	 * @param document
	 * @param path
	 * @return
	 */
	public static String getPathValue(Document document, String path) {
		StringTokenizer st = new StringTokenizer(path, ".");
		Zone zone = null;
		while (st.hasMoreElements()) {
			try {
				if (zone == null) {
					zone = document.getMainZones().stream().filter(z -> z.getCode().equals(st.nextElement()))
							.findFirst().get();
				} else {
					zone = zone.getZones().stream().filter(z -> z.getCode().equals(st.nextElement())).findFirst().get();
				}
			} catch (NoSuchElementException nsee) {
				System.out.println("No such path: " + path);
				System.exit(1);
			}
		}
		return zone.getValue();
	}

	/**
	 * Modifies the XML by replacing the node value found by path
	 * 
	 * @param document
	 * @param path
	 * @param newValue
	 */
	public static void modifyXml(Document document, String path, String newValue) {
		StringTokenizer st = new StringTokenizer(path, ".");
		Zone zone = null;
		// For each element of the path, separated by "."
		while (st.hasMoreElements()) {
			String nextElement = ((String) st.nextElement()).replaceAll("\\[" + ARRAY_ELEMENT_REGEX + "\\]", "");
			Integer nextElementIndex = getPathElementIndex(nextElement);
			try {
				if (zone == null) {
					zone = findZone(document.getMainZones(), nextElement, nextElementIndex);
				} else {
					zone = findZone(zone.getZones(), nextElement, nextElementIndex);
				}
			} catch (NoSuchElementException nsee) {
				System.out.println("No such path: " + path + " " + nsee);
				System.exit(1);
			}
		}
		zone.setValue(newValue);
	}

	/**
	 * Find the zone by code and index if there is one
	 * 
	 * @param zones
	 * @param elementCode
	 * @param elementIndex
	 * @return
	 */
	private static Zone findZone(List<Zone> zones, String elementCode, Integer elementIndex) {
		if (elementIndex != null) {
			return zones.stream().filter(z -> z.getCode().equals(elementCode)).collect(Collectors.toList())
					.get(elementIndex);
		} else {
			return zones.stream().filter(z -> z.getCode().equals(elementCode)).findFirst().get();
		}
	}

	/**
	 * Returns the path element index if there is one
	 * 
	 * @param pathElement
	 * @return
	 */
	private static Integer getPathElementIndex(String pathElement) {
		Matcher matcher = Pattern.compile(ARRAY_ELEMENT_REGEX).matcher(pathElement);
		while (matcher.find()) {
			return Integer.valueOf(matcher.group());
		}
		return null;
	}
}
