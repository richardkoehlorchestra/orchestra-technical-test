package technicaltest;

import java.io.File;

import javax.xml.bind.JAXBException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import technicaltest.jaxb.Document;
import technicaltest.util.DocumentUtil;
import technicaltest.util.XmlUtil;

public class App {

	// Program arguments
	private static final String FILE_OPTION = "f";
	private static final String FILE_LONG_OPTION = "file";
	private static final String PATH_OPTION = "p";
	private static final String PATH_LONG_OPTION = "path";
	private static final String VALUE_OPTION = "v";
	private static final String VALUE_LONG_OPTION = "value";
	private static final String OUTPUT_OPTION = "o";
	private static final String OUTPUT_LONG_OPTION = "output";

	public static void main(String[] args) {
		// Initialize the command line parser and options
		CommandLineParser commandeLineParser = new DefaultParser();
		Options options = new Options();
		Option fileOption = new Option(FILE_OPTION, FILE_LONG_OPTION, true, "Input file");
		Option pathOption = new Option(PATH_OPTION, PATH_LONG_OPTION, true, "Node path");
		Option valueOption = new Option(VALUE_OPTION, VALUE_LONG_OPTION, true, "New value");
		Option outputOption = new Option(OUTPUT_OPTION, OUTPUT_LONG_OPTION, true, "Output file");
		options.addOption(fileOption);
		options.addOption(pathOption);
		options.addOption(valueOption);
		options.addOption(outputOption);

		// Argument values
		String file = null;
		String path = null;
		String value = null;
		String output = null;

		// Parse the command line
		try {
			CommandLine commandLine = commandeLineParser.parse(options, args);
			file = commandLine.getOptionValue(FILE_LONG_OPTION);
			path = commandLine.getOptionValue(PATH_LONG_OPTION);
			value = commandLine.getOptionValue(VALUE_LONG_OPTION);
			output = commandLine.getOptionValue(OUTPUT_LONG_OPTION);
		} catch (ParseException e) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("technical-test", options);
			System.exit(1);
		}

		XmlUtil<Document> xmlUtil = new XmlUtil<>(Document.class);
		// Parse the XML file
		Document document = null;
		try {
			document = (Document) xmlUtil.parse(new File(file));
		} catch (JAXBException e) {
			System.out.println("Error while parsing XML" + " " + e);
			System.exit(1);
		}

		// Determine what to do depending on arguments
		if (value == null || output == null) {
			// If the value and output are null, return the path value
			System.out.println(DocumentUtil.getPathValue(document, path));
		} else if (value != null && output != null) {
			// Modify and output the file
			try {
				// Modify the document
				DocumentUtil.modifyXml(document, path, value);
				// Generate the output XML
				xmlUtil.generateXml(document, new File(output));
			} catch (JAXBException e) {
				System.out.println("Error while generating XML" + " " + e);
				System.exit(1);
			}
		}
	}
}
